import { Router } from "express";
import { helloWorld } from "./controllers/main.controller";

const routes = Router();

routes.get("/", helloWorld);

export default routes;
