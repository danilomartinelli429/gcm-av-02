import app from "./app";
import dotenv from "dotenv";

dotenv.config();

const port = process.env.PORT || 3030;

app.listen(port, () => {
  console.log(`[SERVER] Running at http://localhost:${port}`);
});
